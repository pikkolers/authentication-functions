"use strict";

const request = require("request-promise-native");

async function getAccessTokenUsingOAuthAuthorizationCode(
  authorizationCode,
  tokenUrl,
  loginCallbackUrl,
  clientId,
  clientSecret
) {
  const options = {
    method: "POST",
    uri: tokenUrl,
    headers: {
      "Content-Type": "application/x-www-form-urlencoded"
    },
    form: {
      grant_type: "authorization_code",
      code: authorizationCode,
      redirect_uri: loginCallbackUrl,
      client_id: clientId,
      client_secret: clientSecret
    }
  };

  const result = await request(options);
  const jsonResult = JSON.parse(result);

  const token = jsonResult.access_token;
  const refreshToken = jsonResult.refresh_token;

  return { token, refreshToken };
}

module.exports = getAccessTokenUsingOAuthAuthorizationCode;
