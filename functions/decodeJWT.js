"use strict";
const jwt = require("jsonwebtoken");

module.exports = function(encodedToken) {
  return jwt.decode(encodedToken);
};
