"use strict";

const signJWT = require("./signJWT");

async function generateClientJWT(
  clientId,
  clientName,
  applicationId,
  applicationName,
  { expiresIn = 86400, issuer },
  privateKey
) {
  const tokenSubjectType = "oauth_client";
  const payload = {
    clientId,
    clientName,
    applicationId,
    applicationName,
    subjectType: tokenSubjectType
  };
  // Use assymetric keys to enable
  // verification using only the public key
  const options = {
    algorithm: "RS256",
    issuer,
    expiresIn,
    subject: clientId.toString(),
    audience: "application"
  };
  return await signJWT(payload, options, privateKey);
}

module.exports = generateClientJWT;
