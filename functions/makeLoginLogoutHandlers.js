"use strict";

const randomString = require("./randomString");
const querystring = require("querystring");

const get = require("lodash/get");
const set = require("lodash/set");
const unset = require("lodash/unset");
const isFunction = require("lodash/isFunction");
const { promisify } = require("util");

const InvalidRequestError = require("oauth2-server/lib/errors/invalid-request-error");
const getAccessToken = require("./getAccessTokenUsingOAuthAuthorizationCode");

const decodeJWT = require("./decodeJWT");

const PRODUCTION_ENV = process.env.NODE_ENV === "production";

function makeOAuthAuthorizeUrl(
  oAuthClientId,
  loginCallbackUrl,
  oAuthAuthorizeUrl,
  state,
  successMessage
) {
  const query = querystring.stringify({
    response_type: "code",
    client_id: oAuthClientId,
    redirect_uri: loginCallbackUrl,
    scope: {},
    state,
    successMessage
  });
  return `${oAuthAuthorizeUrl}?${query}`;
}

function defaultSaveOAuthState(state, req) {
  set(req, "session.oAuthState", state);
}

function defaultGetOAuthState(req) {
  return get(req.session, "oAuthState", null);
}

function defaultGetTokenFromSession(req) {
  return get(req.session, "token", null);
}

function defaultSaveToken(token, req) {
  set(req, "session.token", token);
}

function defaultGetAfterLoginUrlOnSession(req) {
  return get(req.session, "afterLoginUrlOnSession", null);
}

function defaultSaveAfterLoginUrlOnSession(url, req) {
  set(req, "session.afterLoginUrlOnSession", url);
}

function defaultDeleteAfterLoginUrlOnSession(req) {
  unset(req, "session.afterLoginUrlOnSession");
}

function getExpiryFromSessionCookie(req) {
  return get(req.session.cookie, "expires");
}

function setLoggedInCookie(res, expires) {
  res.cookie("loggedIn", true, {
    path: "/",
    httpOnly: false,
    expires
  });
}

async function addToUserSessions(client, userId, sessionId) {
  const userSessionsKey = `application_user:sessions:${userId}`;
  const saddAsync = promisify(client.sadd).bind(client);
  return await saddAsync(userSessionsKey, sessionId);
}

async function deleteAllUserSessions(client, userId) {
  const userSessionsKey = `application_user:sessions:${userId}`;

  const smembersAsync = promisify(client.smembers).bind(client);
  const sessionIds = await smembersAsync(userSessionsKey);

  const delAsync = promisify(client.del).bind(client);
  await delAsync(sessionIds.map(id => `sess:${id}`));
  await delAsync(userSessionsKey);

  return true;
}

function makeLoginLogoutHandlers({
  selfOrigin,
  loginCallbackPath = "/oauth/callback",
  loginCallbackUrl = selfOrigin + loginCallbackPath,

  authorizationServerOrigin,
  oAuthAuthorizePath = "/oauth/authorize",
  oAuthAuthorizeUrl = authorizationServerOrigin + oAuthAuthorizePath,
  oAuthTokenPath = "/oauth/token",
  oAuthTokenUrl = authorizationServerOrigin + oAuthTokenPath,

  oAuthClientId,
  oAuthClientSecret,

  saveToken = defaultSaveToken,
  getTokenFromSession = defaultGetTokenFromSession,

  saveOAuthState = defaultSaveOAuthState,
  getOAuthState = defaultGetOAuthState,

  saveAfterLoginUrlOnSession = defaultSaveAfterLoginUrlOnSession,
  getAfterLoginUrlOnSession = defaultGetAfterLoginUrlOnSession,
  deleteAfterLoginUrlOnSession = defaultDeleteAfterLoginUrlOnSession
}) {
  async function getLogin(req, res, next) {
    const { redirect_after_login = "/", successMessage = "" } = req.query;

    const token = getTokenFromSession(req);

    if (!token) {
      // Create and save state parameter for oauth request
      let state = null;
      try {
        state = await randomString();
      } catch (e) {
        return next(e);
      }

      saveOAuthState(state, req);
      saveAfterLoginUrlOnSession(`${selfOrigin}${redirect_after_login}`, req);

      // Redirect to OAuth redirect url
      const urlWithQueryParams = makeOAuthAuthorizeUrl(
        oAuthClientId,
        loginCallbackUrl,
        oAuthAuthorizeUrl,
        state,
        successMessage
      );

      res.redirect(urlWithQueryParams);
    } else {
      const sessionCookieExpiration = getExpiryFromSessionCookie(req);
      //Sync the expiration date of logged in cookie and session cookie
      setLoggedInCookie(res, sessionCookieExpiration);
      res.redirect(`${selfOrigin}${redirect_after_login}`);
    }
  }

  async function getLoginCallback(req, res, next) {
    const { code, state } = req.query;

    let sessionState = null;
    try {
      sessionState = getOAuthState(req);
    } catch (e) {
      console.error(e);
      return next(e);
    }

    if (!code || sessionState !== state) {
      const error = new InvalidRequestError(
        "Invalid request: no authorization code found or state does not match"
      );
      return next(error);
    }

    let accessToken = null;
    try {
      accessToken = await getAccessToken(
        code,
        oAuthTokenUrl,
        loginCallbackUrl,
        oAuthClientId,
        oAuthClientSecret
      );
    } catch (e) {
      const authServerError =
        e.response && isFunction(e.response.toJSON) && e.response.toJSON();
      if (authServerError) {
        const jsonError = JSON.parse(authServerError.body);
        const error = Error(get(jsonError, "error_description"));
        error.code = get(jsonError, "error");
        return next(error);
      } else {
        return next(e);
      }
    }

    try {
      await saveToken(accessToken, req);

      // accessToken = {token: <access token>, refreshToken: <..>}
      const decodedToken = decodeJWT(accessToken.token);
      const userId = decodedToken.sub;

      // Set userId on session
      req.session.userId = userId;

      await addToUserSessions(req.sessionStore.client, userId, req.sessionID);
    } catch (e) {
      return next(e);
    }

    //Get expiration date time from session cookie
    const sessionCookieExpiration = getExpiryFromSessionCookie(req);

    //Sync the expiration date time for the loggedIn cookie with session cookie
    //Expiration date should be set in session config (sessionConfig.cookie.maxAge) of the application session middleware.
    setLoggedInCookie(res, sessionCookieExpiration);

    const redirectToUrlAfterLogin = getAfterLoginUrlOnSession(req);
    if (redirectToUrlAfterLogin) {
      deleteAfterLoginUrlOnSession(req);
      res.redirect(redirectToUrlAfterLogin);
    } else {
      res.redirect(`${selfOrigin}/`);
    }
  }

  async function logout(req, res, next) {
    try {
      await deleteAllUserSessions(req.sessionStore.client, req.session.userId);
    } catch (e) {
      return next(e);
    }

    res.cookie("connect.sid", "", {
      path: "/",
      httpOnly: true,
      expires: new Date(),
      signed: PRODUCTION_ENV
    });
    res.cookie("loggedIn", false, {
      path: "/",
      httpOnly: false,
      expires: new Date()
    });

    res.redirect(`${selfOrigin}/`);
  }

  return {
    getLogin,
    getLoginCallback,
    logout
  };
}

module.exports = makeLoginLogoutHandlers;
