"use strict";

const get = require("lodash/get");
const { createError } = require("apollo-errors");

const InvalidAuthenticationRequestError = createError(
  "InvalidAuthenticationRequestError",
  {
    message: "Invalid authentication request",
    showLocations: false
  }
);
const NotAuthorizedError = createError("NotAuthorizedError", {
  message: "No login credentials provided",
  showLocations: false
});

function getTokenFromRequest(req) {
  const headerToken = req.get("Authorization");
  const queryToken = req.query.access_token;
  const bodyToken = req.body.access_token;

  if (!!headerToken + !!queryToken + !!bodyToken > 1) {
    throw new InvalidAuthenticationRequestError({
      message: "Only one authentication method is allowed."
    });
  }

  let token = null;
  if (headerToken) {
    token = getTokenFromRequestHeader(req);
  }

  if (queryToken) {
    token = queryToken;
  }

  if (bodyToken) {
    token = getTokenFromRequestBody(req);
  }

  if (!token) {
    throw new NotAuthorizedError();
  }

  return token;
}

function getTokenFromRequestHeader(req) {
  const token = req.get("Authorization");
  const matches = token.match(/Bearer\s(\S+)/);

  if (!matches) {
    throw new InvalidAuthenticationRequestError({
      message: "Malformed authorization header"
    });
  }

  return matches[1];
}

function getTokenFromRequestBody(req) {
  if (req.method === "GET") {
    throw new InvalidAuthenticationRequestError({
      message: "Token may not be passed in the body when using the GET verb"
    });
  }

  if (!req.is("application/x-www-form-urlencoded")) {
    throw new InvalidAuthenticationRequestError({
      message: "Content must be application/x-www-form-urlencoded"
    });
  }

  return req.body.access_token;
}

module.exports = getTokenFromRequest;
