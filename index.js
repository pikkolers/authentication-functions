"use strict";

const makeAuthenticationMiddleware = require("./functions/makeAuthenticationMiddleware");
const makeLoginLogoutHandlers = require("./functions/makeLoginLogoutHandlers");
const generateAccountJWT = require("./functions/generateAccountJWT");
const generateClientJWT = require("./functions/generateClientJWT");
const generateManagedJWT = require("./functions/generateManagedJWT");

module.exports = {
  makeAuthenticationMiddleware,
  makeLoginLogoutHandlers,
  generateAccountJWT,
  generateClientJWT,
  generateManagedJWT
};
