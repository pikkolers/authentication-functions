"use strict";
const get = require("lodash/get");
const set = require("lodash/set");
const isFunction = require("lodash/isFunction");
const getTokenFromRequest = require("./getTokenFromRequest");
const decodeJWT = require("./decodeJWT");
const verifyManagedJWT = require("./verifyManagedJWT");
const verifyUnManagedJWT = require("./verifyUnManagedJWT");
const { createError } = require("apollo-errors");

const PRODUCTION_ENV = process.env.NODE_ENV === "production";

const AccessorFetchError = createError("AccessorFetchError", {
  message: "Accessor details could not be fetched.",
  showLocations: false
});
const NotAuthorizedError = createError("NotAuthorizedError", {
  message: "Login credentials are not valid.",
  showLocations: false
});

function defaultGetTokenFromSession(req) {
  return get(req.session, "token", null);
}

function isManagedToken({ aud }) {
  return aud === "issuer";
}

async function defaultDestroySession(req, res) {
  return new Promise((resolve, reject) => {
    res.cookie("connect.sid", "", {
      path: "/",
      httpOnly: true,
      expires: new Date(),
      signed: PRODUCTION_ENV
    });
    res.cookie("loggedIn", false, { path: "/", expires: new Date() });

    if (req.session && typeof req.session.destroy === "function") {
      req.session.destroy(err => {
        if (err) reject(err);
        resolve(null);
      });
    } else {
      resolve(null);
    }
  });
}

function defaultSetEntitiesOnResponse(
  accessorType = null,
  accessor = null,
  accessorGroups = [],
  res
) {
  set(res, "locals.accessor", accessor);
  set(res, "locals.accessorId", accessor.id);

  set(res, "locals.accessorGroups", accessorGroups);
  set(res, "locals.accessorGroupIds", accessorGroups.map(g => g.id));

  set(res, "locals.accessorType", accessorType);
}

function makeAuthenticationMiddleware({
  selfOrigin,
  loginPath,
  loginUrl,

  selfApplicationId,
  selfOAuthClientId,

  authorizationServerPublicKey,
  selfPublicKey,

  getAccountById = id => [],
  getOAuthClientById = id => [],
  getManagedAccessTokenById = id => [],

  getGroupsOfAccount = accountId => [],
  getGroupsOfOAuthClient = oauthClientId => [],

  setEntitiesOnResponse = defaultSetEntitiesOnResponse,

  getTokenFromSession = defaultGetTokenFromSession,
  destroySession = defaultDestroySession
}) {
  async function requireAccessToken(req, res, next) {
    let encodedToken = getTokenFromSession(req);
    let accessToken = get(encodedToken, "token", null);

    if (!accessToken) {
      try {
        accessToken = getTokenFromRequest(req);
      } catch (e) {
        try {
          await destroySession(req, res);
        } catch (e) {
          return next(e);
        }

        return next(e);
      }
    }

    if (!accessToken) {
      const error = new NotAuthorizedError();

      try {
        await destroySession(req, res);
      } catch (e) {
        return next(e);
      }

      if (req.accepts("html")) {
        const redirectToLogin = selfOrigin + loginPath;
        res.redirect(redirectToLogin);
      } else if (req.accepts("json")) {
        res.status(401).json(error);
      } else {
        res.status(401).end(error.message);
      }
    } else {
      res.locals.accessToken = accessToken;
      next();
    }
  }

  async function verifyAccessToken(req, res, next) {
    let encodedToken = getTokenFromSession(req);
    let accessToken = get(encodedToken, "token", null);

    if (!accessToken) {
      try {
        accessToken = getTokenFromRequest(req);
      } catch (e) {
        try {
          await destroySession(req, res);
        } catch (e) {
          return next(e);
        }

        return next(e);
      }
    }

    if (!accessToken) {
      const error = new NotAuthorizedError();

      try {
        await destroySession(req, res);
      } catch (e) {
        return next(e);
      }

      if (req.accepts("html")) {
        const redirectToLogin = selfOrigin + loginPath;
        return res.redirect(redirectToLogin);
      } else if (req.accepts("json")) {
        return res.status(401).json(error);
      } else {
        return res.status(401).end(error.message);
      }
    }

    const decodedToken = decodeJWT(accessToken);
    let isVerifiedToken = null;

    if (isManagedToken(decodedToken)) {
      isVerifiedToken = await verifyManagedJWT(
        accessToken,
        selfPublicKey,
        getManagedAccessTokenById
      );
    } else {
      isVerifiedToken = await verifyUnManagedJWT(
        accessToken,
        { audience: "application" },
        authorizationServerPublicKey
      );

      const { applicationId: tokenApplicationId, clientId } = decodedToken;

      // Verify application to which token was granted
      if (parseInt(tokenApplicationId, 10) !== selfApplicationId) {
        isVerifiedToken = false;
      }

      // FIX ME - is this necessary since we're already validating application
      // Verify client to which token was granted
      // if (clientId !== selfOAuthClientId) {
      //   isVerifiedToken = false;
      // }
    }

    if (!isVerifiedToken) {
      const error = new NotAuthorizedError();

      try {
        await destroySession(req, res);
      } catch (e) {
        return next(e);
      }

      if (req.accepts("html")) {
        return res.status(401).end(error.message);
      } else if (req.accepts("json")) {
        return res.status(401).json(error);
      } else {
        return res.status(401).end(error.message);
      }
    }

    //Logged in cookie need not be reset since the expiration date has been set on the client.
    // res.cookie("loggedIn", true, {
    //   path: "/",
    //   httpOnly: false
    // });

    next();
  }

  async function setAccessorWithGroupMemberships(req, res, next) {
    let encodedToken = getTokenFromSession(req);
    let accessToken = get(encodedToken, "token", null);

    if (!accessToken) {
      try {
        accessToken = getTokenFromRequest(req);
      } catch (e) {
        return next(e);
      }
    }

    if (!accessToken) {
      const error = new NotAuthorizedError();

      try {
        await destroySession(req, res);
      } catch (e) {
        return next(e);
      }

      if (req.accepts("html")) {
        const redirectToLogin = selfOrigin + loginPath;
        return res.redirect(redirectToLogin);
      } else if (req.accepts("json")) {
        return res.status(401).json(error);
      } else {
        return res.status(401).end(error.message);
      }
    }

    const decodedToken = decodeJWT(accessToken);

    const {
      jwtid,
      clientId,
      clientName,
      applicationId,
      applicationName,
      subjectType,
      sub,
      aud
    } = decodedToken;

    let accessorId = null;
    let accessor = null;
    let accessorGroupMemberships = [];
    let getAccessorById = null;
    let getGroupsOfAccessor = null;

    if (subjectType === "account") {
      accessorId = parseInt(sub, 10);
      getAccessorById = getAccountById;
      getGroupsOfAccessor = getGroupsOfAccount;
    } else if (subjectType === "oauth_client") {
      accessorId = sub;
      getAccessorById = getOAuthClientById;
      getGroupsOfAccessor = getGroupsOfOAuthClient;
    } else if (subjectType === "managed_access_token") {
      accessorId = parseInt(sub, 10);
      getAccessorById = getManagedAccessTokenById;
    }

    try {
      accessor = await getAccessorById(accessorId);

      if (isFunction(getGroupsOfAccessor)) {
        accessorGroupMemberships = await getGroupsOfAccessor(accessorId);
      }
    } catch (e) {
      return next(e);
    }

    if (!accessor) {
      const error = new AccessorFetchError({ id: accessorId });
      return next(error);
    }

    setEntitiesOnResponse(subjectType, accessor, accessorGroupMemberships, res);

    next();
  }

  return {
    requireAccessToken,
    verifyAccessToken,
    setAccessorWithGroupMemberships
  };
}

module.exports = makeAuthenticationMiddleware;
