"use strict";
const jwt = require("jsonwebtoken");

module.exports = async function signJWT(payload, options, privateKey) {
  return new Promise((resolve, reject) => {
    jwt.sign(payload, privateKey, options, (err, token) => {
      resolve(null || token);
    });
  });
};
