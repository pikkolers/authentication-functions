"use strict";

const crypto = require("crypto");

module.exports = async function() {
  return await new Promise(function(resolve, reject) {
    crypto.randomBytes(256, (err, buf) => {
      if (err) return reject(err);

      const randomString = crypto
        .createHash("sha1")
        .update(buf)
        .digest("hex");

      resolve(randomString);
    });
  });
};
