"use strict";
const jwt = require("jsonwebtoken");

module.exports = async function verifyUnManagedJWT(
  encodedToken,
  options,
  publicKey
) {
  return new Promise((resolve, reject) => {
    jwt.verify(encodedToken, publicKey, options, (err, decoded) => {
      if (err) {
        console.error(err);
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
};
