"use strict";

const jwt = require("jsonwebtoken");
const decodeJWT = require("./decodeJWT");

async function verifyJWT(encodedToken, options, publicKey) {
  return new Promise((resolve, reject) => {
    jwt.verify(encodedToken, publicKey, options, (err, decoded) => {
      if (err) {
        console.error(err);
        resolve(false);
      } else {
        resolve(true);
      }
    });
  });
}

async function verifyManagedJWT(encodedToken, publicKey, getTokenMetadataById) {
  const verificationOptions = {
    audience: "issuer"
  };
  const isVerifiedJWT = await verifyJWT(
    encodedToken,
    verificationOptions,
    publicKey
  );

  if (!isVerifiedJWT) {
    return false;
  }

  const decodedToken = decodeJWT(encodedToken);

  if (!decodedToken) {
    return false;
  }

  // jti = unique id set for token as `jwtit` while generating the token
  const { jti } = decodedToken;
  const tokenMetadata = await getTokenMetadataById(parseInt(jti, 10));

  if (
    !tokenMetadata ||
    (tokenMetadata && !isValidManagedToken(tokenMetadata))
  ) {
    return false;
  }

  return true;
}

function isValidManagedToken({ maxUsage, currentUsage }) {
  return currentUsage < maxUsage;
}

module.exports = verifyManagedJWT;
