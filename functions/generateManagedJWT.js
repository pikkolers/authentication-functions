"use strict";

const signJWT = require("./signJWT");

async function generateManagedJWT(
  jwtid,
  accountId,
  { expiresIn = 86400, issuer },
  privateKey
) {
  const tokenSubjectType = accountId ? "account" : "managed_access_token";
  const payload = {
    subjectType: tokenSubjectType
  };
  // Use assymetric keys to enable
  // verification using only the public key
  const options = {
    algorithm: "RS256",
    issuer,
    expiresIn,
    jwtid,
    subject: accountId || jwtid || "",
    audience: "issuer"
  };
  return await signJWT(payload, options, privateKey);
}

module.exports = generateManagedJWT;
